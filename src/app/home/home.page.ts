import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

const API_URL = 'https://developers.zomato.com/api/v2.1';
const HTTP_OPTIONS = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'user-key': 'ENTER-YOUR-API-KEY-HERE'
  })
};

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  establishments: any[] = [];
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    const rochesterCityCode = 1003;
    this.getEstablishments(rochesterCityCode);
  }

  // Move all http calls into a new service
  getEstablishments(cityId) {
    const endpointUrl = `${API_URL}/establishments?city_id=${cityId}`;
    this.http.get<any>(endpointUrl, HTTP_OPTIONS)
      .subscribe(
        data => {
          console.log('Server responded with this:');
          console.log(data);
          this.establishments = data.establishments;
        },
        err => {
          console.error('Server responded with an error:');
          console.error(err);
        }
      );

  }

}
