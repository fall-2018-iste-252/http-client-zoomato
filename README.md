# Developer Instructions

1. Clone the project
2. Open `src/app/home/home.page.ts` and update `'user-key': 'ENTER-YOUR-API-KEY-HERE'` with your key.
3. Install dependencies and run in emulator
```bash
cd http-client-zoomato
npm i
ionic cordova emulate browser
```